package chess;

public abstract class Piece {

	private Team team;
	private Multiverse myMulti; // Necessary?

	Piece(Team t, Multiverse m) {
		team = t;
		myMulti = m;
	}

	protected Team getTeam() {
		return team;
	}

	protected abstract int[] possibleMoves();

}
