package chess;

import java.util.ArrayList;

public class GameController {
	
	private Multiverse model;
	private GameView view;
	Team turn;
	ArrayList<Pair> conflicts;
	
	GameController(int sizeOfBoard, int numOfBoards) {
		assert(sizeOfBoard >= 3 && sizeOfBoard <= 10);
		model = new Multiverse(sizeOfBoard, numOfBoards);
		view = new GameView();
		turn = Team.WHITE;
	}
	
	private boolean compareTeam(Team t1, Team t2) {
		return t1.compareTo(t2) == 0;
	}
	
	private void updateTurn() {
		turn = compareTeam(turn, Team.WHITE) ? Team.BLACK : Team.WHITE;
	}
	
	private void updateView() {
		view.update(model);
	}
	
	private boolean sameTurnPieceTeam(int timelineIndex, int srcPiece) {
		Team pieceTeam = model.getPiece(timelineIndex, srcPiece).getTeam();
		return compareTeam(turn, pieceTeam);
	}
	
	private boolean canAttackKing(Piece attacker, int attackerIndex, Team team) {
		boolean canAttackKing = false;
		
		int[] unsafeSpaces = attacker.possibleMoves();
		CoordinateList dList = model.getDefenseList(team);
		
		for (int i = 0; i < unsafeSpaces.length; i++)
			if (dList.has(unsafeSpaces[i])) {
				canAttackKing = true;
				conflicts.add(new Pair(attackerIndex, unsafeSpaces[i]));
			}
		
		return canAttackKing;
	}
	
	private boolean inCheck(Team team) {
		boolean inCheck = false;
		
		ArrayList<Timeline> timelines = model.getMultiverse();
		for (int i = 0; i < timelines.size(); i++) {
			Timeline checkTimeline = timelines.get(i);
			Board presentBoard = checkTimeline.getLastBoard();
			for (int row = 0; row < presentBoard.getState().length; row++)
				for (int column = 0; column < presentBoard.getState().length; column++) {
					int attackerIndex = (checkTimeline.getOffset() + checkTimeline.getLength() - 1) * 100000;
					//TODO attackerIndex += stuff * 100;
					attackerIndex += row * 10;
					attackerIndex += column;
					Piece piece = presentBoard.getPieceAt(row * 10 + column);
					
					if (piece != null && piece.getTeam().compareTo(team) != 0)
						if (canAttackKing(piece, attackerIndex, team))
							inCheck = true;
				}
		}
		
		return inCheck;
	}
	
	private boolean inCheckmate(Team team) {
		
		ArrayList<Timeline> timelines = model.getMultiverse();
		for (int i = 0; i < timelines.size(); i++) {
			Timeline checkTimeline = timelines.get(i);
			Board presentBoard = checkTimeline.getLastBoard();
			for (int row = 0; row < presentBoard.getState().length; row++)
				for (int column = 0; column < presentBoard.getState().length; column++) {
					Piece piece = presentBoard.getPieceAt(row * 10 + column);
					if (piece != null && piece.getTeam().compareTo(team) == 0)
						for (int coordinate : piece.possibleMoves()) {
							int destTimeline = coordinate / 100000;
							int destBoard = (coordinate % 100000) / 100;
							int destRow = (coordinate % 100) / 10;
							int destColumn = coordinate % 10;
							
							movePiece(i, row, column, destTimeline, destBoard, destRow, destColumn);
							if (!inCheck(team)) {
								model.revert();
								return false;
							} else {
								model.revert();
							}
						}
				}
		}
		
		return true;
	}
	
	private void endGame() {
		//TODO
	}
	
	//TODO Bridge with GUI
	public int getTurn() {
		return turn.compareTo(Team.WHITE);
	}
	
	// Add Piece with name pieceName on Team t to timeline # timelineIndex at Row row and Column column
	public void addPieceToBoard(String pieceName, Team t, int timelineIndex, int row, int column) {
		Piece newPiece = pieceFactory.createPiece(pieceName, t, model);
		model.addPiece(timelineIndex, row, column, newPiece);
	}
	
	// Move Piece from timeline # timelineIndex at row srcRow and column srcColumn to row destRow and column destColumn
	public void movePiece(int timelineIndex, int srcRow, int srcColumn, int destRow, int destColumn) {
		int srcPiece = srcRow * 10 + srcColumn;
		int destPiece = destRow * 10 + destColumn;
		
		if (!sameTurnPieceTeam(timelineIndex, srcPiece)) { return; }
		
		model.move(timelineIndex, srcPiece, destPiece);
		
		if (inCheck(turn)) {
			model.revert();
			conflicts.clear();
		} else {			
			updateTurn();
			if (inCheck(turn)) {
				if (inCheckmate(turn))
					endGame();
				conflicts.clear();
			}
		}
		
		updateView();
	}
	
	// Move Piece from timeline # timelineIndex at row srcRow and column srcColumn to row destRow and column destColumn
	// on Board # from the timeline
	public void movePiece(int timelineIndex, int srcRow, int srcColumn, int destBoard, int destRow, int destColumn) {
		int srcPiece = srcRow * 10 + srcColumn;
		int destPiece = destRow * 10 + destColumn;
		
		if (!sameTurnPieceTeam(timelineIndex, srcPiece)) { return; }
		
		model.move(timelineIndex, srcPiece, destBoard, destPiece);
		
		if (inCheck(turn)) {
			model.revert();
			conflicts.clear();
		} else {			
			updateTurn();
			if (inCheck(turn)) {
				if (inCheckmate(turn))
					endGame();
				conflicts.clear();
			}
		}
		
		updateView();
	}

	// Move Piece from timeline # srcTimeline at row srcRow and column srcColumn to row destRow and column destColumn
		// on Board # from the destTimeline
	public void movePiece(int srcTimeline, int srcRow, int srcColumn, int destTimeline, int destBoard, int destRow, int destColumn) {
		int srcPiece = srcRow * 10 + srcColumn;
		int destPiece = destRow * 10 + destColumn;
		
		if (!sameTurnPieceTeam(srcTimeline, srcPiece)) { return; }
		
		model.move(srcTimeline, srcPiece, destTimeline, destBoard, destPiece);
	
		if (inCheck(turn)) {
			model.revert();
			conflicts.clear();
		} else {			
			updateTurn();
			if (inCheck(turn)) {
				if (inCheckmate(turn))
					endGame();
				conflicts.clear();
			}
		}
		
		updateView();
	}

}
