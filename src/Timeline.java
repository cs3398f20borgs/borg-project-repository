package chess;

import java.util.ArrayList;

class Timeline {

	private ArrayList<Board> timeline;
	private final int offset;

	// Regular Constructor
	Timeline(Board b) {
		timeline = new ArrayList<Board>();
		timeline.add(b);
		offset = 0;
	}

	// Constructor for branch timeline
	Timeline(Board b, Piece p, int dest, int offset) {
		timeline = new ArrayList<Board>();
		timeline.add(new Board(b, p, dest));
		this.offset = offset;
	}

	ArrayList<Board> getTimeline() {
		return timeline;
	}
	
 	int getOffset() {
		return offset;
	}

	Board getBoard(int boardIndex) {
		return timeline.get(boardIndex);
	}

	Board getLastBoard() {
		return timeline.get(timeline.size() - 1);
	}

	Piece getPieceFromLast(int src) {
		return getLastBoard().getPieceAt(src);
	}

	int getLength() {
		return timeline.size();
	}

	void subBoard(int src) {
		timeline.add(new Board(getLastBoard(), src));
	}
	
	void addBoard(int src, int dest) {
		timeline.add(new Board(getLastBoard(), src, dest));
	}

	void addBoard(Piece p, int dest) {
		timeline.add(new Board(getLastBoard(), p, dest));
	}
	
	// Method to add to Piece to newly initialized Board
	void addPiece(int row, int column, Piece newPiece) {
		timeline.get(0).addPiece(row, column, newPiece);
	}
	
}
