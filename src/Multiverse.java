package chess;

import java.util.ArrayList;

public class Multiverse {
	
	private ArrayList<Timeline> multiverse;
	private CoordinateList blackDefenseList;
	private CoordinateList whiteDefenseList;
	private int boardBackup;
	private int timelineBackup;
	private int multiBackup;
	int balance;
	
	Multiverse(int size, int count) {
		multiverse = new ArrayList<Timeline>();
		
		for (int i = 0; i < count; i++) {
			multiverse.add(new Timeline(new Board(size)));
		}
		
		boardBackup = -1;
		timelineBackup = -1;
		multiBackup = -1;
		balance = 0;
	}

	ArrayList<Timeline> getMultiverse() {
		return multiverse;
	}
	
	CoordinateList getDefenseList(Team team) {
		if (team.compareTo(Team.BLACK) == 0)
			return blackDefenseList;
		return whiteDefenseList;
	}
	
	Piece getPiece(int srcTime, int srcPiece) {
		return multiverse.get(srcTime).getPieceFromLast(srcPiece);
	}
	
	// Method to add Piece to newly initialize Board
	void addPiece(int timelineIndex, int row, int column, Piece newPiece) {
		multiverse.get(timelineIndex).addPiece(row, column, newPiece);
	}

	void addTimeline(Board b, Piece p, int dest, int off) {
		multiverse.add(new Timeline(b, p, dest, off));
	}

	// method for regular piece movement
	void move(int srcTime, int srcPiece, int destPiece) {
		multiverse.get(srcTime).addBoard(srcPiece, destPiece);
		
		boardBackup = srcTime;
		timelineBackup = -1;
		multiBackup = -1;
	}

	// method for time-traveling piece movement
	void move(int srcTime, int srcPiece, int destBoard, int destPiece) {
		Piece p = getPiece(srcTime, srcPiece);
		multiverse.get(srcTime).subBoard(srcPiece);
		
		addTimeline(multiverse.get(srcTime).getBoard(destBoard), p, destPiece,
				(multiverse.get(srcTime).getOffset() + destBoard));
		
		boardBackup = srcTime;
		timelineBackup = multiverse.size() - 1;
		multiBackup = -1;
		if (p.getTeam().compareTo(Team.BLACK) == 0) {
			balance++;
		} else {
			balance--;
		}
	}
	
	// method for multiverse-traveling piece movement
	void move(int srcTime, int srcPiece, int destTime, int destBoard, int destPiece) {
		Piece p = getPiece(srcTime, srcPiece);
		multiverse.get(srcTime).subBoard(srcPiece);
		
		if (multiverse.get(destTime).getLength() - 1 == destBoard) {
			multiverse.get(destTime).addBoard(p, destPiece);
			
			boardBackup = srcTime;
			timelineBackup = -1;
			multiBackup = destTime;
		} else {
			addTimeline(multiverse.get(destTime).getBoard(destBoard), p, destPiece,
				(multiverse.get(destTime).getOffset() + destBoard));
			
			boardBackup = srcTime;
			timelineBackup = multiverse.size() - 1;
			multiBackup = -1;
			if (p.getTeam().compareTo(Team.BLACK) == 0) {
				balance++;
			} else {
				balance--;
			}
		}
	}
	
	void revert() {
		if (boardBackup != -1) {
			int lastBoard = multiverse.get(boardBackup).getTimeline().size();
			multiverse.get(boardBackup).getTimeline().remove(lastBoard);
			boardBackup = -1;
		}
		
		if (timelineBackup != -1) {
			multiverse.remove(timelineBackup);
			timelineBackup = -1;
		}
		
		if (multiBackup != -1) {
			int lastBoard = multiverse.get(multiBackup).getTimeline().size();
			multiverse.get(multiBackup).getTimeline().remove(lastBoard);
			multiBackup = -1;
		}
	}
	
}
