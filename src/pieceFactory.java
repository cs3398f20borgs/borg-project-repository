package chess;

public class pieceFactory {

	public static Piece createPiece(String name, Team t, Multiverse m) {
		switch (name.toLowerCase()) {
		case "king":
			return new King(t, m);
		case "queen":
			return new Queen(t, m);
		case "bishop":
			return new Bishop(t, m);
		case "rook":
			return new Rook(t, m);
		case "knight":
			return new Knight(t, m);
		case "pawn":
			return new Pawn(t, m);
		case "custom":
			// TODO
		default:
			return null;
		}
	}

}
