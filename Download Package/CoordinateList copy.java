package chess;

import java.util.ArrayList;
import java.util.Collections;

public class CoordinateList {
	private ArrayList<Integer> dList;

	void add(int coordinate) {
		dList.add(coordinate);
		Collections.sort(dList);
	}

	boolean has(int coordinate) {
		Integer iInteger = Integer.valueOf(coordinate);
		return (dList.indexOf(iInteger) != -1);
	}

}