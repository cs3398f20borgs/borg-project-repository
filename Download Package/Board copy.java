package chess;

class Board {

	private Piece[][] board;

	// Regular constructor
	Board(int size) {
		board = new Piece[size][size];
	}

	// Constructor with piece movement
	Board(Board b, int src, int dest) {
		cloneBoard(b);
		board[dest / 10][dest % 10] = board[src / 10][src % 10];
		board[src / 10][src % 10] = null;
	}

	// Constructor with piece removal
	Board(Board b, int src) {
		cloneBoard(b);
		board[src / 10][src % 10] = null;
	}

	// Constructor with piece addition
	Board(Board b, Piece p, int dest) {
		cloneBoard(b);
		board[dest / 10][dest % 10] = p;
	}
	
	void setState(Piece[][] state) {
		for (int i = 0; i < state.length; i++)
			board[i] = state[i].clone();
	}
	
	Piece[][] getState() {
		return board;
	}
	
	void addPiece(int row, int column, Piece newPiece) {
		board[row][column] = newPiece;
	}
	
	Piece getPieceAt(int coordinate) {
		return board[coordinate / 10][coordinate % 10];
	}
	
	void cloneBoard(Board b) {
		board = new Piece[b.getState().length][];
		this.setState(b.getState());
	}
	
}
