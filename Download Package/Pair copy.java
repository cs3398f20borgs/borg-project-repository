package chess;

public class Pair {

	private final int key;
	private final int value;
	
	Pair(int k, int v) {
		key = k;
		value = v;
	}
	
	int getKey() {
		return key;
	}
	
	int getValue() {
		return value;
	}
	
}
