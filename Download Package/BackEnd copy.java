package FrontEndGui;

public class BackEnd {
	public boolean move(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		switch (board[startX][startY]) {
		case 0 : case 10 : return king(board, startX, startY, landingX, landingY, whiteTurn);
		case 1 : case 11 : return queen(board, startX, startY, landingX, landingY, whiteTurn);
		case 2 : case 12 : return rook(board, startX, startY, landingX, landingY, whiteTurn);
		case 3 : case 13 : return bishop(board, startX, startY, landingX, landingY, whiteTurn);
		case 4 : case 14 : return knight(board, startX, startY, landingX, landingY, whiteTurn);
		case 5 : case 15 : return pawn(board, startX, startY, landingX, landingY, whiteTurn);
		default : return false;
		}
	}
	
	private boolean pawn(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		int xDif =  landingX - startX;
		int yDif =  landingY - startY;
		if (startX == landingX) {
			if ((whiteTurn ? -yDif : yDif) > 0) {
				if (Math.abs(yDif) == 1 && board[landingX][landingY] == -1) {
					return true;
				}else if (Math.abs(yDif) == 2 && board[landingX][landingY] == -1 &&
						((whiteTurn ? board[landingX][landingY+1] : board[landingX][landingY-1]) == -1) && startY == (whiteTurn ? 6 : 1)) {
					return true;
				}
			}
		}else if(Math.abs(xDif) == 1) {
			if (whiteTurn) {
				if (yDif == -1 && board[landingX][landingY] >= 10) {
					return true;
				}
			}else {
				if (yDif == 1 && board[landingX][landingY] >= 0 && board[landingX][landingY] < 10) {
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean knight(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		if (board[landingX][landingY] == -1 || (whiteTurn && board[landingX][landingY]>= 10) 
				|| (!whiteTurn && board[landingX][landingY] < 10)) {
			int xDif =  Math.abs(landingX - startX);
			int yDif =  Math.abs(landingY - startY);
			if ((xDif == 1 && yDif == 2) || (xDif == 2 && yDif == 1)) return true;
		}
		return false;
	}
	
	private boolean rook(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		int xDif =  landingX - startX;
		int yDif =  landingY - startY;
		if (xDif == 0) {
			if (yDif > 0) {
				for (int  i = startY+1; i < landingY; i++) {
					if (board[landingX][i] != -1) {
						return false;
					}
				}
				return true;
			}else {
				for (int  i = landingY+1; i < startY; i++) {
					if (board[landingX][i] != -1) {
						return false;
					}
				}
				return true;
			}
		}else if(yDif == 0) {
			if (xDif > 0) {
				for (int  i = startX+1; i < landingX; i++) {
					if (board[i][landingY] != -1) {
						return false;
					}
				}
				return true;
			}else {
				for (int  i = landingX+1; i < startX; i++) {
					if (board[i][landingY] != -1) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	private boolean bishop(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		int xDif =  landingX - startX;
		int yDif =  landingY - startY;
		if (Math.abs(xDif) == Math.abs(yDif)) {
			for (int i = 1; i < Math.abs(xDif); i++) {
				if (board[startX + (i * (xDif > 0 ? 1 : -1))][startY + (i * (yDif > 0 ? 1 : -1))] != -1) {
					return false;
				}
			}
			return true;
		}
		
		return false;
	}
	
	private boolean queen(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		if (bishop(board, startX, startY, landingX, landingY, whiteTurn)) return true;
		if (rook(board, startX, startY, landingX, landingY, whiteTurn)) return true;
		return false;
	}
	
	private boolean king(int[][] board, int startX, int startY, int landingX, int landingY, boolean whiteTurn) {
		int xDif =  Math.abs(landingX - startX);
		int yDif =  Math.abs(landingY - startY);
		if (xDif <= 1 && yDif <= 1)return true;
		return false;
	}
}
