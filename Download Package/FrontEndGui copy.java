package FrontEndGui;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.net.URL;
import java.util.HashMap;


public class FrontEndGui {
    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private JPanel Board;
    private JToolBar options;
    private final Image[][] chessPieceSprites = new Image[2][6];
    private final JButton[][] BoardSquares = new JButton[8][8];
    private final JLabel Statements = new JLabel ("2D Chess!");
    private static final String FILL = "ABCDEFGH";
    public final static int King=0, Queen=1, Rook=2,Bishop=3,Knight=4,Pawn=5;
    public static int[] Piece_Placement={Rook, Knight, Bishop,  Queen, King, Bishop, Knight, Rook};

    
    HashMap<MouseListener,int[]> buttonPos = new HashMap<MouseListener,int[]>();
    
 SimpleAudioPlayer audioPlayer;

    int selectedX = -1, selectedY = -1;
    int landedX = -1, landedY = -1;

    boolean whiteTurn = true;
    boolean startedGame;
    public int[][] board = new int[8][8];
    
    BackEnd backend = new BackEnd();
    
public static final int BlackTeam=0, WhiteTeam=1;

 public static void main(String[] args) {
        Runnable runGame = new Runnable() {
            public void run() {
                FrontEndGui gui5D = new FrontEndGui();
                JFrame f = new JFrame("Chess");
                f.add(gui5D.getGui());
                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                f.setLocationByPlatform(true);
                f.pack();
                f.setMinimumSize(f.getSize());
                f.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(runGame);
    }
 
public FrontEndGui() {   
    
    //if it is not running due to some vague exception exit this for no sound.
//  /*
try {
		audioPlayer = new SimpleAudioPlayer(getFile());
	} catch (UnsupportedAudioFileException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (LineUnavailableException e) {
		e.printStackTrace();
	}
// */

    OpenGui();   
}

private File getFile() {
	File file = null;
	String resource = "chimes.wav";
	URL res = getClass().getResource(resource);
	if (res.getProtocol().equals("jar")) {
	    try {
	        java.io.InputStream input = getClass().getResourceAsStream(resource);
	        file = File.createTempFile("tempfile", ".tmp");
	        FileOutputStream out = new FileOutputStream(file);
	        int read;
	        byte[] bytes = new byte[1024];

	        while ((read = input.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	        out.close();
	        file.deleteOnExit();
	    } catch (IOException ex) {
	    }
	} else {
	    //this will probably work in your IDE, but not from a JAR
	    file = new File(res.getFile());
	}
	
	return file;
}

  public JPanel getBoard() {
     return Board;
    }
  public JToolBar getTools(){
      return options;
  }


    private void OpenGui() {
        getImagePieces();                                                           // get image of chess pieces
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));           // main GUI
        options = new JToolBar();
        options.setFloatable(false);
        gui.add(options, BorderLayout.PAGE_START); 
       options.add(newAction);                                         //initialize action listeners for option buttons
        options.add(helpAction);
         options.add(exitAction);
        options.add(Statements);
       
        
        //line border layout of chess board
        gui.add(new JLabel("2D CHESS"), BorderLayout.LINE_START);
        Board = new JPanel(new GridLayout(0,9)) {
        private static final long serialVersionUID = -161316812933934130L; };
        Board.setBorder(new CompoundBorder(
        new EmptyBorder(8,8,8,8),
        new LineBorder(Color.BLACK) ));
        
        // set background color
        Color lavender = new Color(200,200,250);
        Board.setBackground(lavender);
        JPanel boardSize = new JPanel(new GridBagLayout());
        boardSize.setBackground(lavender);
        boardSize.add(Board);
        gui.add(boardSize);
        
        // creates board squares
        Insets SquareSize = new Insets(0, 0, 0, 0);
        for (int i = 0; i < BoardSquares.length; i++) {
        for (int j = 0; j < BoardSquares[i].length; j++) {
        JButton SquareButton = new JButton();
        SquareButton.setMargin(SquareSize);
        MouseListener m = new MouseListener() {
			public void mouseClicked(MouseEvent arg0) {}
			public void mouseEntered(MouseEvent arg0) {}
			public void mouseExited(MouseEvent arg0) {}
			public void mousePressed(MouseEvent arg0) {}
			public void mouseReleased(MouseEvent arg0) {
				int[] pos = buttonPos.get(this);
				if (startedGame)selectButton(pos[0],pos[1]);
			}
        };
        
        buttonPos.put(m, new int[]{i,j});
        //  chess pieces are 64x64 px in size
      // this gets the sprite and makes it transparent to make it pink or purple
                ImageIcon Sprite = new ImageIcon(
                new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB));
                SquareButton.setIcon(Sprite);
                if ((j % 2 == 1 && i % 2 == 1)
                    || (j % 2 == 0 && i % 2 == 0)) {
                    SquareButton.setBackground(Color.WHITE);
                } else {
                    SquareButton.setBackground(Color.BLACK);
                }
                
                SquareButton.addMouseListener(m);
                
                BoardSquares[j][i] = SquareButton;
            }
        }

 // then fill the chess board
 Board.add(new JLabel(""));
        // fill top row
        for (int i = 0; i < 8; i++) {
            Board.add(new JLabel(FILL.substring(i, i + 1),
            SwingConstants.CENTER));
        }
        
        // fill BLACK non-pawn piece row
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                switch (j) {
                    case 0:
                    Board.add(new JLabel("" + (9-(i + 1)),
                    SwingConstants.CENTER));
                    default:
                    Board.add(BoardSquares[j][i]);
                }
            }
        }
    }
 public final JComponent getGui() {
 return gui;
    }

    // gets image default chess pieces from url in 64x64  size
 public void getImagePieces() {
        try {
            URL url = new URL("https://i.imgur.com/FrAzm8T.png");
            BufferedImage pieces = ImageIO.read(url);
            for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 6; j++) {
            chessPieceSprites[i][j] = pieces.getSubimage(j * 64, i *64, 64, 64);
                }
            }
        } 
        catch (IOException e) {
            System.exit(1);
        }
    }
 
 //help menu button method
   private void HelpMenu(){
        Component frame = null;
        JOptionPane.showInternalMessageDialog(frame, "The chessboard is made up of eight rows and eight columns for a total of 64 squares of alternating colors. Each square of the chessboard is identified with a unique pair of a letter and a number", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "King can move exactly one square horizontally, vertically, or diagonally. At most once in every game, each king is allowed to make a special move, known as castling", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "Queen can move any number of vacant squares diagonally, horizontally, or vertically.", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "Rook can move any number of vacant squares vertically or horizontally. It also is moved while castling.", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "Bishop can move any number of vacant squares in any diagonal direction.", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "Knight can move one square along any rank or file and then at an angle. The knight´s movement can also be viewed as an “L” or “7″ laid out at any horizontal or vertical angle..", "Help", JOptionPane.INFORMATION_MESSAGE);
        JOptionPane.showInternalMessageDialog(frame, "Pawns can move forward one square, if that square is unoccupied. If it has not yet moved, the pawn has the option of moving two squares forward provided both squares in front of the pawn are unoccupied. A pawn cannot move backward.)", "Help", JOptionPane.INFORMATION_MESSAGE);
    }
    
    
   
  
   // places sprites when pressing start game
    private void StartGame() {
        Statements.setText("Have Fun playing Chess!");
        
        board = new int[8][8];
        
        // black pieces
        for (int i = 0; i < Piece_Placement.length; i++) {
        	board[i][0] = Piece_Placement[i]+11;
            BoardSquares[i][0].setIcon(new ImageIcon(
                    chessPieceSprites[BlackTeam][Piece_Placement[i]]));
        }
        for (int i = 0; i < Piece_Placement.length; i++) {
        	board[i][1] = Pawn+11;
            BoardSquares[i][1].setIcon(new ImageIcon(
                    chessPieceSprites[BlackTeam][Pawn]));
        }
        
        // white pieces
        for (int i = 0; i < Piece_Placement.length; i++) {
        	board[i][6] = Pawn+1;
            BoardSquares[i][6].setIcon(new ImageIcon(
                    chessPieceSprites[WhiteTeam][Pawn]));
        }
        for (int i = 0; i < Piece_Placement.length; i++) {
        	board[i][7] = Piece_Placement[i]+1;
            BoardSquares[i][7].setIcon(new ImageIcon(
                    chessPieceSprites[WhiteTeam][Piece_Placement[i]]));
        }
        
        for(int i = 0; i < 8; i++) {
        	for (int j = 2; j < 6; j++) {
        		BoardSquares[i][j].setIcon(null);
        	}
        }
        
        for (int i = 0; i < 8; i++) {
        	for (int j = 0; j < 8; j++) {
        		board[i][j]--;
        	}
        }
        if (selectedX != -1) {
    		BoardSquares[selectedX][selectedY].setBorder(UIManager.getBorder("Button.border"));
    		selectedX = -1; selectedY = -1;
        }
        if (landedX!=-1) {
    		BoardSquares[landedX][landedY].setBorder(UIManager.getBorder("Button.border"));
    		landedX = -1; landedY = -1;
    	}
        whiteTurn = true;
        startedGame = true;
               audioPlayer.play();
    }
    
    private void selectButton(int y, int x) {
    	if (landedX != -1) {
    		BoardSquares[selectedX][selectedY].setBorder(UIManager.getBorder("Button.border"));
    		BoardSquares[landedX][landedY].setBorder(UIManager.getBorder("Button.border"));
    		landedX = -1; landedY = -1;
    		selectedX = -1; selectedY = -1;
    	}
    	if (selectedX != -1) {
    		if ((board[x][y] >= 10 && !whiteTurn) || (board[x][y] < 10 && whiteTurn)  && board[x][y] >= 0) {
    			BoardSquares[selectedX][selectedY].setBorder(UIManager.getBorder("Button.border"));
    			selectedX = -1; selectedY = -1;
    		}
    	}
    	if (selectedX == -1) {
    		if ((board[x][y] >= 10 && !whiteTurn) || (board[x][y] < 10 && whiteTurn)  && board[x][y] >= 0) {
        		selectedX = x;
        		selectedY = y;
        		BoardSquares[x][y].setBorder(BorderFactory.createLineBorder(Color.yellow));
        		
        	}
    	}else {
    		movePiece(selectedX, selectedY, x, y);
    	}
    }
    
    private void movePiece(int startX, int startY, int landingX, int landingY) {
    	if (startX == landingX && startY == landingY) return;
    	if (backend.move(board, startX, startY, landingX, landingY, whiteTurn)) {
    		if (board[landingX][landingY] == ((whiteTurn) ? (King+10) : King)) {
    	        Statements.setText(((whiteTurn) ? "Purple" : "Pink") + " Won!");
    	        startedGame = false;
    		}
    		int piece = board[startX][startY];
			board[startX][startY] = -1;
			board[landingX][landingY] = piece;
    		BoardSquares[landingX][landingY].setBorder(BorderFactory.createLineBorder(Color.green));
    		BoardSquares[landingX][landingY].setIcon(BoardSquares[startX][startY].getIcon());
    		BoardSquares[startX][startY].setIcon(null);
    		landedX = landingX;
    		landedY = landingY;
    		whiteTurn = !whiteTurn;
    	}
    }
    
    
//action listeners     
        Action newAction = new AbstractAction("New") {
            //override to start game
            @Override
            public void actionPerformed(ActionEvent e) {
            StartGame();
            }
        };
        
              Action helpAction = new AbstractAction("Help") {
            //override for help menu
            @Override
            public void actionPerformed(ActionEvent e) {
                   HelpMenu();
            }
        };
              
    Action exitAction = new AbstractAction("Quit Game"){
                          //override for exiting game
            @Override
            public void actionPerformed (ActionEvent e) {
  System.exit(0);
 }
};
}

class SimpleAudioPlayer  { 
  
    Long currentFrame; 
    Clip clip; 
    String status; 
    AudioInputStream audioInputStream; 
  
    public SimpleAudioPlayer(File file) 
        throws UnsupportedAudioFileException, 
        IOException, LineUnavailableException  
    { 
        audioInputStream =  
                AudioSystem.getAudioInputStream(file.getAbsoluteFile()); 
        clip = AudioSystem.getClip();
    }
    
    public void play() {
    	try {
			clip.open(audioInputStream);
		} catch (LineUnavailableException | IOException e) {
			e.printStackTrace();
		} 
        clip.loop(30); 
    }
}




