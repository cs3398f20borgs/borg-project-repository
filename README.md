# 5D Chess
> Team - Joe Armstrong, Zachary Boemer, Berker Erdini, Cameron Paul, Randy Silva  
> A website that hosts a chess game with extra dimensions of movement and slightly altered rules
> for chess enthusiast
> to provide an unorthodox and interesting twitst on the class game of chess

## Table of contents
- [5D Chess](#5d-chess)
  - [Table of contents](#table-of-contents)
  - [General info](#general-info)
  - [Logo](#logo)
  - [Technologies](#technologies)
  - [Code Examples](#code-examples)
  - [Features](#features)
  - [Status](#status)
  - [Inspiration](#inspiration)
  - [Contact](#contact)

## General info
The purpose of this project is to demonstrate good programming practice (inclduing SOLID principles and effective desgin pattern implementation), and practical skills 
through the implementation of the game on a website.

## Logo
![Logo](https://files.slack.com/files-pri/T018E63PLCS-F01B48M418S/logo.png)

## Technologies
* JAVA Eclipse, NetBeans.
* PHP, Server comunication to update page while playing game.
* HTML, CSS, and Javascript to decorate GUI and to communicate with PHP.
* Bitbucket and Jira to help the team with organization and the flow of the project.
* Java Applets to embed the game on the website.

## Code Examples
Method for Factory Pattern Piece creation
```java
public static Piece createPiece(String name, Team t, Multiverse m) {  
	switch (name.toLowerCase()) {  
    case "king":  
    	return new King(t, m);  
    case "queen":  
        return new Queen(t, m);  
    case "bishop":  
        return new Bishop(t, m);  
    case "rook":  
        return new Rook(t, m);  
    case "knight":  
        return new Knight(t, m);  
    case "pawn":  
        return new Pawn(t, m);  
    case "custom":  
        // TODO  
    default:  
        return null;  
    }  
}  
```

Method for Piece movement through GameController class
```java
public void movePiece(int timelineIndex, int srcRow, int srcColumn, int destRow, int destColumn) {
		int srcPiece = srcRow * 10 + srcColumn;
		int destPiece = destRow * 10 + destColumn;
		
		if (!sameTurnPieceTeam(timelineIndex, srcPiece)) { return; }
		
		model.move(timelineIndex, srcPiece, destPiece);
		
		if (inCheck(turn)) {
			model.revert();
			conflicts.clear();
		} else {			
			updateTurn();
			if (inCheck(turn)) {
				if (inCheckmate(turn))
					endGame();
				conflicts.clear();
			}
		}
		
		updateView();
}
```


part of OpenGui method for determining chess piece color/team (currently black/white) by getting the obtained png and using modular arithmetic to obtain the pieces location to determine the team a piece is on.
```java
private void OpenGui() {

        Insets SquareSize = new Insets(0, 0, 0, 0);
        for (int i = 0; i < BoardSquares.length; i++) {
        for (int j = 0; j < BoardSquares[i].length; j++) {
        JButton SquareButton = new JButton();
        SquareButton.setMargin(SquareSize);
    
    //  chess pieces are 64x64 px in size
      // this gets the sprite and makes it transparent to make it white or black
                ImageIcon Sprite = new ImageIcon(
                new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB));
                SquareButton.setIcon(Sprite);
                if ((j % 2 == 1 && i % 2 == 1)
                    || (j % 2 == 0 && i % 2 == 0)) {
                    SquareButton.setBackground(Color.WHITE);
                } else {
                    SquareButton.setBackground(Color.BLACK);
                }
                BoardSquares[j][i] = SquareButton;
            }
        }
```


## Features

- Board Model, a 2D collection of Piece objects for a Timeline object, user story model chess board  
    - Artifact src/Board.java

- Timeline Model, an expanding List of a Board object and all its previous states for a Multiverse object, user story model 5D chess board  
    - Artifact src/Timeline.java  

- Multiverse Model, a collection of Timeline objetcs encompassing all Board states including methods for interaction, user story model 5D chess board  
    - Artifact src/Multiverse.java  

- Piece Model, a template for specifcis chess pieces (e.g., King, Queen, etc.), user story model chess pieces  
    - Artifacts src/Team.java src/Piece.java src/pieceLibrary.java src/pieceFactory.java  

- Controller Model, a class that controls the access to the game model and updates the view accordingly
    - Artifact src/GameController.java

- Website template for the game
    - Artifacts webpage/index.html webpage/style.css
    - Learnt advance web development and design methods

- Piece Template for what team a specific team is on
	- Artifacts FrontEndGui/FrontEndGui.java
    
- Download file for Browsers that do not support Java Applets
    - Artifacts develop webpage/DownloadFile.zip

## Status
Project is: _done_

## Accomplishments
- Berker created the official website for our game Chess Dimension, using HTML, CSS, Javascript technologies.
- Cameron made an applet that would display the game online. Technology to use Applet is Java 8. He also created a download package for the game.
- Randy made a Frontend GUI for regular 2D Chess and also made piece movement algorithms for the chess pieces and linked it with the frontend.
## Inspiration
Project inspired by _5D Chess with Multiverse Time Travel_ Copyright (c) 2020 Thunkspace, LLC

## Contact
Created by [@Joe Armstrong](), [@Zachary Boemer](), [@Berker Erdini](), [@Cameron Paul](), [@Randy Silva]()
